# eu estou copiando a wiki pra esse readme
# Ruby_Wiki

## Índice: 

 - #### 1. O que é Ruby
 - #### 2 Filosofia
 - #### 3 História
   - 3.1. Principais versões de Ruby
 - #### 4. Características da Linguagem
   - 4.1 Ruby, uma linguagem multiparadigma
   - 4.2 Variáveis
   - 4.3 Pilhas e Recursão
   - 4.4 Estruturas de controle em Ruby
   - 4.5 Sentenças iterativas


## 1. O que é Ruby


Ruby é uma linguagem de programação de propósito geral, interpretada e de alto nível. Ruby foi desenvolvida nos anos 90 por Yukihiro "Matz" Matsumoto.

Ruby é de tipagem dinâmica e usa garbage collector. A linguagem oferece suporte a vários paradigmas de programação, incluindo orientação a objetos, procedural e funcional. Segundo "Matz" o criador da linguagem, as linguagens que o influenciaram ao criar Ruby foram Perl, Smalltalk, Eiffel, Ada, and Lisp.

## 2.Filosofia


A filosofia que é usada para desenvolver Ruby é um aspecto fundamental para a linguagem, por isso é importante comentar sobre esse assunto.Ruby foi projetado com a satisfação do programador e a produtividade em mente, Matz comentou sobre a filosofia de ruby em uma Google Tech Talk, dizendo: "Espero ver Ruby ajudando programadores ao redor do mundo a serem produtivos, a desfrutar da programação e a serem felizes. Esse é o objetivo principal da linguagem Ruby." 

Muitos dizem que o que guia o desenvolvimento de Ruby é princípio da menor surpresa, isso quer dizer que a sintaxe de Ruby tenta evitar confusões, e para alcançar isso, uma das estratégias é manter a sintaxe o mais familiar e natural possível, Matz comentou sobre esse princípio e disse que não aplicou esse princípio em Ruby, inclusive ele tenta afastar a linguagem dessa ideia dizendo que não existe motivo para o design de uma linguagem "surpreender" alguém, mesmo assim a linguagem é constantemente associada com esse princípio.

Na verdade Matz disse que seu principal objetivo de design era fazer uma linguagem que ele mesmo gostasse de usar, minimizando confusão e minimizando também o trabalho do programador.

## 3.História


A ideia de criar Ruby foi concebida em 1993 segundo Matz, ele estava conversando com um colega sobre a possibilidade de uma linguagem de script orientada a objetos nessa época programação orientada a objetos estava em ascensão, Matz queria uma linguagem de script fácil de usar e por mais que Perl e Python se encaixariam nesse padrão Matz estava satisfeito e resolveu criar sua própria linguagem.

A primeira publicação de Ruby foi Ruby 0.95 em 1995, Por volta de 2005, o interesse pela linguagem Ruby aumentou junto com Ruby on Rails, um framework da web escrito em Ruby. Rails é frequentemente creditado com o aumento da percepção do Ruby.    

Desde então Ruby atraiu muitos desenvolvedores dedicados, em 2004 a Gem "on Rails" foi lançada o que ajudou a aumentar a popularidade de Ruby.

Ruby está classificado entre os 10 primeiros na maioria dos índices que medem o crescimento e a popularidade das linguagens de programação em todo o mundo (como o índice TIOBE).

### 3.1 Principais versões de Ruby

| Versão | Data de lançamento |Comentários sobre a versão| 
| ------ | ------ | ------ |
| Ruby 0.95 | 1995 | Foi publicada em um jornal local. |  
| Ruby 1.0 | Dezembro de 1996 | Em 1997 Matz foi contratado pela netlab.jp para trabalhar full-time em ruby. |
| Ruby 1.2  | Dezembro de 1998 |Ruby Application Archive foi lançado junto com uma página simples na internet em inglês para a linguagem.|
| Ruby 1.6 | Setembro de 2000 | Durante os anos 2000 Ruby se tornou mais popular que Python no Japão e Ruby começou a ganhar popularidade em outros países, além disso o primeiro livro em inglês sobre Ruby foi publicado. |
| Ruby 1.8 | agosto de 2003 | Só foi aposentado em junho de 2013 pois nessa versão ruby já era robusto o suficiente para se tornar um dos padrões usados na indústria. |
| Ruby 1.9 | Natal de 2007 | Ruby 1.9 era parcialmente retro-compatível com Ruby 1.8, o que afetou a adoção da nova versão, muitas gems tiveram que ser reescritas para a versão 1.9. |
| Ruby 2.0 | fevereiro de 2013 | Não era completamente retro-compatível com a versão 1.9. |
| Ruby 2.2 | natal de 2014 | Além de otimizações e bugfixes, essa versão introduziu mudanças na forma que ruby administra memória, melhorou o garbage collector. |

**A versão atual de Ruby é a 2.7.2 e Ruby 3.0 está previsto para ser lançado esse ano.**
  

## 4 Características da Linguagem

Ruby uma linguagem orientada a objetos, mas diferente da maioria das linguagens em ruby tudo é um objeto, incluindo classes e tipos que a maioria das linguagens definem como tipos primitivos como por exemplo int, boolean e null.

Ruby trabalha com métodos mas esses métodos não precisam de uma classe e basicamente atuam como funções, por isso Ruby suporte o paradigma estrutural, os métodos que são chamados fora de um objeto tornam-se métodos da classe Object, e como essa classe é a classe pai de todas as outras classes, esses métodos podem ser chamados em qualquer objeto, servindo como uma função, mas servindo efetivamente como procedimentos "globais". Ruby suporta herança com despacho dinâmico, mixins e métodos singleton (pertencentes a, e definidos para, uma única instância ao invés de serem definidos na classe). Embora Ruby não suporte herança múltipla, as classes podem importar módulos como mixins.

 ### 4.1 Ruby, uma linguagem multiparadigma

Ruby permite a programação procedural (implementa funções/variáveis ​​fora das classes torna-as parte da raiz, 'self' Object), com orientação a objetos (tudo é um objeto) ou programação funcional (tem funções anônimas, closures e continuations). Ruby não suporta programação lógica nativamente, mas suporta com o uso de certas bibliotecas como a biblioteca Russell apresentada nesse vídeo:([https://www.youtube.com/watch?v=f5Bi6_GOIB8](url)).

|  |Sobre o vídeo que apresenta a biblioteca Russell|
| ------ | ------ |
| 5:47 | Gavin McGimpsey explica os três conceitos na base de programação lógica: Declaração, Relação e Inferência, então ele dá um exemplo usando um exercício simples de como Ruby pode ser Declarativo. |
| 9:15 | ele começa a explicação de como Ruby pode exercer o conceito de Relações. |
| 12:16 | começa a explicação de como Ruby exerce inferência usando 2 exercícios: n Queens e Sudoku  |
| 18:49 | McGimpsey começa a apresentar o Russell que é uma biblioteca de que introduz uma linguagem lógica que ele criou.Ele comenta antes que já existem bibliotecas que traduzem linguagens lógicas mais conhecidas como prolog. |

### 4.2 Variáveis

Ruby guarda variáveis globais em heap, as variáveis locais entram em heap mas são desalocadas assim que deixam de ser usadas, depois que as variáveis globais são utilizados os dados dos objetos são deslocados da heap e substituídos por uma referencia ao objeto em questão, ruby usa uma heap manager em C chamado C heap para manipular a heap, se a heap estiver usando muita memória o Ruby automaticamente roda um garbage collection eventualmente.

O tamanho limite padrão da heap em Ruby é 10000 slots. A heap aumenta de acordo com o uso, quando a heap precisa de mais memória ela irá usar o heap growth factor para definir quanto de memória ela irá crescer toda vez que faltar memória e esse valor pode ser configurado com os seguintes comandos:

    RUBY_HEAP_MIN_SLOTS= 
    <define a quantidade inicial de slots na heap>

    RUBY_HEAP_SLOTS_INCREMENT= 
    <define a quantidade de slots que serão alocados na primeira o programa precisar>
    
    RUBY_HEAP_SLOTS_GROWTH_FACTOR= 
    <define o fator de crescimento da heap nas a partir da segunda vez que o programa        
    precisar de mais slots, esse valor por default é 1.8, o cálculo é feito a partir da 
    quantidade inicial de slots, se o programa tem inicialmente 10000 slots na segunda vez 
    que ele for incrementar ele irá alocar 18000 slots (`10000 * 1.8`) a próxima vês que ruby 
    precisar alocar slots o cálculo é a partir da quantidade de slots que foram alocados da 
    ultima vês, ou seja 32400 slots (`18000 * 1.8`)>

    RUBY_GC_MALLOC_LIMIT= 
    <define a quantidade máxima de slots que podem ser alocados sem que o garbage collector 
    seja acionado>

    RUBY_HEAP_FREE_MIN= 
    <define o numero minimo de slots que devem ser liberados depois que o garbage collector é 
    acionado>

##### Tipos de variáveis em Ruby:

##### Variável Global:

    ruby> $foo
       nil
    ruby> $foo = 5
       5
    ruby> $foo
       5


##### Variável Instância:

     class InstTest
           def set_foo(n)
             @foo = n
           end
           def set_bar(n)
             @bar = n
           end
         end
       nil
     i = InstTest.new
       #<InstTest:0x83678>
     i.set_foo(2)
       2
     i
       #<InstTest:0x83678 @foo=2>
     i.set_bar(4)
       4
     i
       #<InstTest:0x83678 @foo=2, @bar=4>


##### Variável Local:

     bar=0
       0
     p1 = proc{|n| bar=n}
       #<Proc:0x8deb0>
     p2 = proc{bar}
       #<Proc:0x8dce8>
     p1.call(5)
       5
     bar
       5
     p2.call
       5

##### Nomes de identificadores 

Identificadores em ruby são case sensitive e usam caracteres alfanuméricos e underscore mas devem sempre começar com uma letra ou underscore, não existem restrições de tamanho para identificadores em ruby

Expressões regulares são delimitadas com a barra. o caractere que vem depois da barra define a opção da expressão regular, ‘i’ por exemplo quer dizer expressão regular case insensitive.

Exemplo:
        /^Ruby the OOPL/
	/Ruby/i
	/my name is #{myname}/o
	%r|^/usr/local/.*|

Uma das vantagens de ruby tem haver com a filosofia de desenvolvimento, que tenta manter a linguagem o mais proxima da natual e isso extende as palavras reservadas.

      if true then
        puts "the test resulted in a true-value"
      end

Todas as palavras reservadas nesse caso são usadas de forma que o código é similar a uma frase em inglês.

##### Estilos

Variáveis locais são nomeadas com letras minúsculas começando com uma letra ou underscore. O nome de uma classe declarada dentro de uma classe começa com “@@” camelCase é bem comum na comunidade de Ruby, mas o mais comum é separar palavras com underscore '_'.
      
      sunil, _z, hit_and_run
      @@sign, @@_, @@Counter

##### Tipos de variáveis

variáveis globais podem ser acessadas de qualquer parte do programa.
são identificadas pelo sinal ‘$’ antes do nome

variáveis instance pertencem a um certo objeto 
são identificadas pelo sinal ’@’antes do nome

variáveis locais são instanciadas somente em um bloco e só podem ser chamadas nesse bloco.
são identificadas por não terem um símbolo antes do nome

pseudo variáveis são variáveis com especiais reservadas, por exemplo:
self, self, true, false


#### Variáveis de pilha

Ruby libera memória de variáveis locais (de pilha) depois delas serem utilizadas, mas guarda uma referência para onde ela foi salva no disco para o caso dela ser usada novamente até o fim do programa, isso é uma das causas de um dos problemas com a linguagem que é o alto uso de memória.



### 4.3 Pilhas e Recursão


A pilha é uma estrutura de dados em que você insere e retira dados no topo de uma pilha, e a recursão em ruby cria uma pilha para manter os endereços de memória que irão receber os dados quando a recursão acha o valor, funciona como uma lista de tarefas, um método recursivo guarda dentro de uma pilha os endereços que irão receber os resultados, quando a recursão acha o resultado do último elemento da pilha ele retorna o resultado para o elemento no topo da pilha, retira esse elemento e retorna o resultado para o próximo elemento até que a pilha esteja vazia. A quantidade de memória que a pilha pode usar antes de receber um erro pode ser redefinido em ruby, o tamanho máximo padrão que uma pilha pode ter em ruby é normalmente é 10080, mas você vai ter mais ou menos esse valor dependendo do interpretador e do sistema, o meu por exemplo é 10918. Esse valor pode ser redefinido com o seguinte comando: 

`export RUBY_THREAD_VM_STACK_SIZE=<quantidade de elementos>`

SystemStackError é o equivalente a stack overflow em ruby, esse é um erro que acontece quando o programa usa uma pilha maior do que o tamanho máximo que foi definido.

Aqui um exemplo de um codigo que resulta em SystemStackError no meu computador:


    def test(x)
        while (x<10918) do
             return test(x+1)
        end
        return x
    end
    test(1)


Uma forma de resolver nesse caso seria usar o comando 

`export RUBY_THREAD_VM_STACK_SIZE=2097152`

ele aumentar o tamanho máximo que a pilha pode ter para 2mb

#### Tail Call

Tail Call em Ruby só funcionava com a implementação de um TCO (tail call optimization), que faz tail call funcionar como um loop, e era preciso habilitar o TCO criando uma flag de compilação em tempo de execução.

    # fact.rb
    def fact(n, acc=1)
      return acc if n <= 1
      fact(n-1, n*acc)
    end
    # main.rb
    RubyVM::InstructionSequence.compile_option = {
      tailcall_optimization: true,
      trace_instruction: false
    }
    require './fact.rb'`
    fact(1000)


### 4.4 Estruturas de controle em Ruby

Ruby tem as estruturas de controle comuns, como if e while. E em vez de usar colchetes para definir o escopo Ruby tem a palavra chave “end”.

Ex.:

     if count > 10
       puts "Try again"
     elsif tries == 3
       puts "You lose"
     else
       puts "Enter a number"
     end

Existem 3 instruções de seleção em Ruby (case, while e for).

Ex.:   

##### Case:

     i=8
     case i
       when 1, 2..5
         print "1..5\n"
       when 6..10
         print "6..10\n"
     end

##### While: 

     i = 0
     num = 5

     while i < num  do
        puts("Está dentro do while" )
        i +=1
     end

##### For:
     for num in (4..6)
        print num,"\n"
     end

A regra para aninhamento de instruções de seleção em Ruby diz que o seletor deve sempre ser fechado com a palavra chave “end”, e isso inclusive evita ambiguidades no caso de seletores aninhados.

Ruby possui dois tipos de instrução de seleção múltipla o `case` por exemplo é similar ao switch case em c mas com mais flexibilidade em relação ao controle de casos, Ruby permite subfaixas ou range como de 1 a 99: “1..99”,  e não possui saída implícita.

Ex.:   
     i=8
     case i
          when 1, 2..5
            print "1..5\n"
          when 6..10
            print "6..10\n"
     end


Ruby também tem o “elseif” que é outro tipo de seleção múltipla que tem uma flexibilidade maior em relação ao tipo de caso e também não possui saída implícita.

Ex.:   
     if x > 2
        puts "x is greater than 2"
     elsif x <= 2 and x!=0
        puts "x is 1"
     else
        puts "I can't guess the number"
     end



Mas, em versões mais recentes Ruby faz Tail Call sem necessidade de um TCO.


### 4.5 Sentenças iterativas

Ruby tem o Bloco que quando definido pode ser usado novamente usando “yield”. Ruby também possui Iterators, que são métodos que entram em loop sobre um determinado conjunto de dados e se repetem em cada elemento da coleção.

Ex.:  
##### Loop baseado em contador:

     names = ['Bob', 'Joe', 'Steve', 'Janice', 'Susan', 'Helen']
     x = 1
     names.each do |name|
      puts "#{x}. #{name}"
       x += 1
     end
##### Loop controlado logicamente: 

     def print_twice
       yield
       yield
     end
     print_twice { puts "Hello" }

##### Controle de loop definido pelo usuário

Ruby suporta break e next. Break serve para sair do loop antes de achar um resultado e next para pular o próximo comando.

Beak:
     loop do
       break if count == 10
       puts count
       count += 1
     end
Next:
     strings.inject(0) do |sum, str|
       next if str.size == 4
       sum + str.size
     end

##### Subprogramas 

Ruby é uma linguagem orientada a objetos, e assim como outras linguagens do tipo os subprogramas em Ruby são chamados de métodos, mas diferente de outras linguagens orientadas a objetos Ruby permite que um método seja declarado fora de uma classe, fazendo com que esse método funciona essencialmente como uma função em linguagem estrutural. Ruby suporta passagem de parâmetros entre subprogramas, mas como em Ruby tudo é objeto, o que o programa está fazendo é passagem de objetos, ruby passa parâmetros por referência e o interpretador verifica o tipo do parâmetro então não é necessário definir. 

Ex.:  
     def mais3 (x)
       return mais1(mais1(mais1(x)))
     end
     def mais2 (x)
       return mais1(mais1(x))
     end
     def mais1(x)
       return x+1
     end
     def mais4 (x)
       x=mais1 (x)
       x=mais2 (x)
       x=mais3 (x)
       return x
     end
     puts (mais4(1))

Ruby também suporta passagem de subprogramas como parâmetros: 

      def subpr(x)
        return mais1(mais1(x))
      end
      def mais1(x)
        return x+1
      end      
      x=subpr(1)
      puts(x)
